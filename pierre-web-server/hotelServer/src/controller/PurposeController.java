package controller;

import java.io.IOException;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import entity.Purpose;
import manager.PurposeManager;

@WebServlet("/purposes")
public class PurposeController extends HttpServlet {

	private static final long serialVersionUID = 1L;

	public PurposeController() {
		super();
	}

	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {

		Purpose[] purposes;

		if (request.getParameter("id") != null && !request.getParameter("id").isEmpty())
			purposes = PurposeManager.getById(request.getParameter("id"));
		else
			purposes = PurposeManager.getAll();

		response.getWriter().append(Purpose.serialize(purposes));

	}

	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		doGet(request, response);
	}

}
