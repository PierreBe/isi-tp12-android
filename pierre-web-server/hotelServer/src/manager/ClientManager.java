package manager;

import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;

import org.eclipse.jdt.annotation.Nullable;

import controller.Resources;
import entity.Client;
import service.DBConnexion;

public class ClientManager {

	static private final String TABLE_NAME = "clients";

	static private final String OP_SELECT = Resources.OP_SELECT;
	static private final String OP_EQUALS = Resources.OP_EQUALS;
	static private final String OP_REPLACE = Resources.OP_REPLACE;

	static private final String CLAUSE_FROM = Resources.CLAUSE_FROM;
	static private final String CLAUSE_WHERE = Resources.CLAUSE_WHERE;

	static private final String COL_ALL = Resources.COL_ALL;
	static private final String COL_ID = Resources.COL_ID;
	static private final String COL_ID_ROOM = "idRoom";

	static private final String END_QUERY = Resources.END_QUERY;

	static private final String QUERY_GET_BY_EMAIL_AND_PWD = OP_SELECT + " " + COL_ALL + " " + CLAUSE_FROM + " " + TABLE_NAME + " " + CLAUSE_WHERE + " " + "email" + OP_EQUALS + OP_REPLACE + " " + "and" + " " + "pwdHash" + OP_EQUALS + OP_REPLACE + END_QUERY;

	static public Client authenticate(String email, String pwd) {
		Client[] ca = get(QUERY_GET_BY_EMAIL_AND_PWD, email, pwd);
		if (ca.length == 1)
			return ca[0];
		else
			return null;
	}

	static private final String INSERT_NEW_CLIENT = "insert into clients (firstName,lastName,email,pwdHash) values (?,?,?,?);";

	static private final String UPDATE_CLIENT = "update clients set firstName=?,lastName=?,email=?,pwdHash=? where id=? and pwdHash=?;";

	static public final Client createOrUpdate(String serializedClient, String pwd, @Nullable String newPwd) {
		Client c = Client.deserializeItem(serializedClient);
		if (c.getId() == -1) {
			if (get("select * from clients where email=?;", c.getEmail()).length != 0 || !validateClientAndPwd(c, pwd) || update(INSERT_NEW_CLIENT, c.getFirstName(), c.getLastName(), c.getEmail(), pwd) != 1) {
				c = null;
			} else {
				c = authenticate(c.getEmail(), pwd);
			}
		} else {
			if (!validateClientAndPwd(c, pwd) || !validateStrings(newPwd) || update(UPDATE_CLIENT, c.getFirstName(), c.getLastName(), c.getEmail(), (newPwd == null || newPwd.isEmpty() ? pwd : newPwd), "" + c.getId(), pwd) != 1)
				c = null;
		}
		return c;
	}

	// PRIVATE METHODS :

	static private final Client[] get(String query, String... stringParams) {
		ArrayList<Client> clients = new ArrayList<Client>();
		try {
			PreparedStatement ps = DBConnexion.getPs(query);
			if (stringParams != null)
				for (int i = 0; i < stringParams.length; i++)
					ps.setString(i + 1, stringParams[i]);
			ps.executeQuery();
			ResultSet rs = ps.getResultSet();
			while (rs.next())
				clients.add(new Client(rs.getInt(1), rs.getString(2), rs.getString(3), rs.getString(4)));
		} catch (SQLException e) {
			e.printStackTrace();
		}
		DBConnexion.close();
		Client[] ca = new Client[clients.size()];
		return clients.toArray(ca);
	}

	static private final int update(String query, String... stringParams) {
		int r = 0;
		try {
			PreparedStatement ps = DBConnexion.getPs(query);
			if (stringParams != null)
				for (int i = 0; i < stringParams.length; i++)
					ps.setString(i + 1, stringParams[i]);
			r = ps.executeUpdate();
		} catch (SQLException e) {
			e.printStackTrace();
		}
		DBConnexion.close();
		return r;
	}

	static private boolean validateClientAndPwd(Client c, String pwd) {
		return validateStrings(c.getFirstName(), c.getLastName(), c.getEmail(), pwd);
	}

	static private boolean validateStrings(String... strings) {
		boolean b = true;
		for (String s : strings)
			if (s != null && (s.contains(Resources.SEPARATOR_PARAMETER) || s.contains(Resources.SEPARATOR_ARRAY))) {
				b = false;
				break;
			}
		return b;
	}

}
