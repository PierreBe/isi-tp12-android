package controller;

import java.io.IOException;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import entity.Hotel;
import manager.HotelManager;

@WebServlet("/hotels")
public class HotelController extends HttpServlet {

	private static final long serialVersionUID = 1L;

	public HotelController() {
		super();
	}

	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {

		Hotel[] hotels;

		if (request.getParameter("id") != null)
			hotels = HotelManager.getById(request.getParameter("id"));
		else if (request.getParameter("city") != null)
			hotels = HotelManager.getByCity(request.getParameter("city"));
		else if (request.getParameter("state") != null)
			hotels = HotelManager.getByState(request.getParameter("state"));
		else if (request.getParameter("country") != null)
			hotels = HotelManager.getByCountry(request.getParameter("country"));
		else
			hotels = HotelManager.getAll();

		response.getWriter().append(Hotel.serialize(hotels));

	}

	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		doGet(request, response);
	}

}
