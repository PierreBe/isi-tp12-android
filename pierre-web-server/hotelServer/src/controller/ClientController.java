package controller;

import java.io.IOException;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import entity.Client;
import manager.ClientManager;

@WebServlet("/clients")
public class ClientController extends HttpServlet {

	private static final long serialVersionUID = 1L;

	public ClientController() {
		super();
	}

	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {

		Client c = null;

		if (request.getParameter("email") != null && !request.getParameter("email").isEmpty() && request.getParameter("pwd") != null && !request.getParameter("pwd").isEmpty())
			c = ClientManager.authenticate(request.getParameter("email"), request.getParameter("pwd"));
		else if (request.getParameter("client") != null && !request.getParameter("client").isEmpty() && request.getParameter("pwd") != null && !request.getParameter("pwd").isEmpty())
			c = ClientManager.createOrUpdate(request.getParameter("client"), request.getParameter("pwd"), request.getParameter("newPwd"));

		response.getWriter().append(c == null ? null : Client.serialize(c));

	}

	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		doGet(request, response);
	}

}
