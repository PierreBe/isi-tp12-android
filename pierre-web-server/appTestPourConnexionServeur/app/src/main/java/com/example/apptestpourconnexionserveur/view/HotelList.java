package com.example.apptestpourconnexionserveur.view;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;

import com.example.apptestpourconnexionserveur.R;
import com.example.apptestpourconnexionserveur.entity.Hotel;

import java.util.List;

public class HotelList extends ArrayAdapter<Hotel> {

    private Context ctx;
    private int idLayout;

    public HotelList(@NonNull Context context, int resource, @NonNull List objects) {
        super(context, resource, objects);
        ctx = context;
        idLayout = resource;
    }

    @NonNull
    @Override
    public View getView(int position, @Nullable View convertView, @NonNull ViewGroup parent) {

        Hotel h = getItem(position);

        if (convertView == null)
            convertView = LayoutInflater.from(ctx).inflate(idLayout, null);

        ((TextView) convertView.findViewById(R.id.tv_id)).setText("" + h.getId());
        ((TextView) convertView.findViewById(R.id.tv_name)).setText("" + h.getName());
        ((TextView) convertView.findViewById(R.id.tv_phone)).setText("" + h.getPhoneNumber());
        ((TextView) convertView.findViewById(R.id.tv_email)).setText("" + h.getEmail());
        ((TextView) convertView.findViewById(R.id.tv_addr)).setText("" + h.getAddrL1());
        ((TextView) convertView.findViewById(R.id.tv_zip)).setText("" + h.getZip());
        ((TextView) convertView.findViewById(R.id.tv_city)).setText("" + h.getCity());
        ((TextView) convertView.findViewById(R.id.tv_state)).setText("" + h.getState());
        ((TextView) convertView.findViewById(R.id.tv_country)).setText("" + h.getCountry());

        return convertView;
    }
}
