package manager;

import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.Map;

import controller.Resources;
import entity.Hotel;
import entity.Room;
import service.DBConnexion;

public class RoomManager {

	static private final boolean LOG_QUERY = false;

	static private final String TABLE_NAME = "rooms";

	static private final String OP_SELECT = Resources.OP_SELECT;
	static private final String OP_EQUALS = Resources.OP_EQUALS;
	static private final String OP_REPLACE = Resources.OP_REPLACE;

	static private final String CLAUSE_FROM = Resources.CLAUSE_FROM;
	static private final String CLAUSE_WHERE = Resources.CLAUSE_WHERE;

	static private final String COL_ALL = Resources.COL_ALL;
	static private final String COL_ID = Resources.COL_ID;
	static private final String COL_CITY = Resources.COL_CITY;
	static private final String COL_STATE = Resources.COL_STATE;
	static private final String COL_COUNTRY = Resources.COL_COUNTRY;

	static private final String END_QUERY = Resources.END_QUERY;

	/*static private final String QUERY_GET_ALL = OP_SELECT + " " + COL_ALL + " " + CLAUSE_FROM + " " + TABLE_NAME + END_QUERY;
	
	static public Room[] getAll() {
		return get(QUERY_GET_ALL);
	}
	
	static private final String QUERY_GET_BY_ID = OP_SELECT + " " + COL_ALL + " " + CLAUSE_FROM + " " + TABLE_NAME + " " + CLAUSE_WHERE + " " + COL_ID + " " + OP_EQUALS + " " + OP_REPLACE + END_QUERY;
	
	static public final Room[] getById(String id) {
		return get(QUERY_GET_BY_ID, id);
	}
	
	// -- select rooms.* from rooms inner join hotels on rooms.idHotel=hotels.id where hotels.city='Québec';
	
	// -- select * from rooms where idHotel in (select id from hotels where city='Québec');
	
	static private final String QUERY_GET_BY_CITY = OP_SELECT + " " + COL_ALL + " " + CLAUSE_FROM + " " + TABLE_NAME + " " + CLAUSE_WHERE + " " + "idHotel in (select id from hotels where city=?);"; // + COL_CITY + " " + OP_EQUALS + " " + OP_REPLACE + END_QUERY;
	
	static public final Room[] getByCity(String city) {
		return get(QUERY_GET_BY_CITY, city);
	}
	
	static private final String QUERY_GET_BY_STATE = OP_SELECT + " " + COL_ALL + " " + CLAUSE_FROM + " " + TABLE_NAME + " " + CLAUSE_WHERE + " " + "idHotel in (select id from hotels where state=?);"; // + COL_STATE + " " + OP_EQUALS + " " + OP_REPLACE + END_QUERY;
	
	static public final Room[] getByState(String state) {
		return get(QUERY_GET_BY_STATE, state);
	}
	
	static private final String QUERY_GET_BY_COUNTRY = OP_SELECT + " " + COL_ALL + " " + CLAUSE_FROM + " " + TABLE_NAME + " " + CLAUSE_WHERE + " " + "idHotel in (select id from hotels where country=?);"; // + COL_COUNTRY + " " + OP_EQUALS + " " + OP_REPLACE + END_QUERY;
	
	static public final Room[] getByCountry(String country) {
		return get(QUERY_GET_BY_COUNTRY, country);
	}
	
	static private final String QUERY_GET_BY_ID_HOTEL = OP_SELECT + " " + COL_ALL + " " + CLAUSE_FROM + " " + TABLE_NAME + " " + CLAUSE_WHERE + " " + "idHotel" + " " + OP_EQUALS + " " + OP_REPLACE + END_QUERY;
	
	static public final Room[] getByIdHotel(String idHotel) {
		return get(QUERY_GET_BY_ID_HOTEL, idHotel);
	}
	
	static private final String QUERY_GET_BY_ID_VIEW = OP_SELECT + " " + COL_ALL + " " + CLAUSE_FROM + " " + TABLE_NAME + " " + CLAUSE_WHERE + " " + "idView" + " " + OP_EQUALS + " " + OP_REPLACE + END_QUERY;
	
	static public final Room[] getByIdView(String idView) {
		return get(QUERY_GET_BY_ID_VIEW, idView);
	}
	
	static private final String QUERY_GET_BY_BED_NUMBER = OP_SELECT + " " + COL_ALL + " " + CLAUSE_FROM + " " + TABLE_NAME + " " + CLAUSE_WHERE + " " + "singleBedNumber" + " " + ">=" + " " + OP_REPLACE + " " + "and" + " " + "doubleBedNumber" + " " + ">=" + " " + OP_REPLACE + END_QUERY;
	
	static public final Room[] getByBedNumber(String singleBedNumber, String doubleBedNumber) {
		return get(QUERY_GET_BY_BED_NUMBER, singleBedNumber, doubleBedNumber);
	}
	
	static private final String QUERY_GET_BY_PRICE = OP_SELECT + " " + COL_ALL + " " + CLAUSE_FROM + " " + TABLE_NAME + " " + CLAUSE_WHERE + " " + "price" + " " + ">=" + " " + OP_REPLACE + " " + "and" + " " + "price" + " " + "<=" + " " + OP_REPLACE + END_QUERY;
	
	static public final Room[] getByPrice(String priceFloor, String priceCeiling) {
		return get(QUERY_GET_BY_PRICE, priceFloor, priceCeiling);
	}
	
	static private final String QUERY_GET_BY_SMOKER = OP_SELECT + " " + COL_ALL + " " + CLAUSE_FROM + " " + TABLE_NAME + " " + CLAUSE_WHERE + " " + "smoker" + " " + OP_EQUALS + " " + OP_REPLACE + END_QUERY;
	
	static public final Room[] getBySmoker(String smoker) {
		return get(QUERY_GET_BY_SMOKER, smoker.equals("true") ? "1" : "0");
	}*/

	static private final String QUERY_BEGIN = OP_SELECT + " " + COL_ALL + " " + CLAUSE_FROM + " " + TABLE_NAME;

	static private final String QUERY_MIDDLE_1 = " " + CLAUSE_WHERE + " " + "idHotel in (select id from hotels where ";

	static private final String QUERY_PARAM = "! = ?";

	static private final String QUERY_MIDDLE_2 = ")";

	static private final String[] LEGIT_PARAM_NAMES_HOTELS = { "city", "state", "country" };

	static private final String QUERY_MIDDLE_3 = "id not in (select idRoom from bookings where ";

	static private final String[] LEGIT_PARAM_NAMES_BOOKINGS = { "fromDate", "toDate" };

	static private final String[] LEGIT_PARAM_NAMES_ROOMS = { "id", "idHotel", "idView", "singleBedNumber", "doubleBedNumber", "priceFloor", "priceCeiling", "smoker" };

	static public final Room[] getByAnyParameter(Map<String, String[]> map) {
		String query = QUERY_BEGIN;
		ArrayList<String> kal = new ArrayList<String>();
		ArrayList<String> val = new ArrayList<String>();

		boolean atLeastOneKeyFoundInFirstArray = false;

		for (String key : map.keySet()) {
			for (String param : LEGIT_PARAM_NAMES_HOTELS) {
				if (key.equals(param) && !map.get(key)[0].isEmpty()) {
					atLeastOneKeyFoundInFirstArray = true;
					query += QUERY_MIDDLE_1;
					break;
				}
			}
			if (atLeastOneKeyFoundInFirstArray)
				break;
		}

		boolean firstParamEntered = false;

		if (atLeastOneKeyFoundInFirstArray) {
			for (String key : map.keySet()) {
				for (String param : LEGIT_PARAM_NAMES_HOTELS) {
					if (key.equals(param) && !map.get(key)[0].isEmpty()) {
						if (firstParamEntered)
							query += " and ";
						else
							firstParamEntered = true;
						query += QUERY_PARAM;
						kal.add(key);
						val.add(map.get(key)[0]);
					}
				}
			}
		}

		if (atLeastOneKeyFoundInFirstArray)
			query += QUERY_MIDDLE_2;

		boolean atLeastOneKeyFoundInSecondArray = false;

		for (String key : map.keySet()) {
			for (String param : LEGIT_PARAM_NAMES_BOOKINGS) {
				if (key.equals(param) && !map.get(key)[0].isEmpty()) {
					atLeastOneKeyFoundInSecondArray = true;
					break;
				}
			}
			if (atLeastOneKeyFoundInSecondArray)
				break;
		}

		if (atLeastOneKeyFoundInFirstArray && atLeastOneKeyFoundInSecondArray)
			query += " and ";
		else if (atLeastOneKeyFoundInSecondArray)
			query += " where ";
		if (atLeastOneKeyFoundInSecondArray)
			query += QUERY_MIDDLE_3;

		firstParamEntered = false;

		if (atLeastOneKeyFoundInSecondArray) {
			for (String key : map.keySet()) {
				for (String param : LEGIT_PARAM_NAMES_BOOKINGS) {
					if (key.equals(param) && !map.get(key)[0].isEmpty()) {
						if (firstParamEntered)
							query += " and ";
						else
							firstParamEntered = true;
						if (key.equals(LEGIT_PARAM_NAMES_BOOKINGS[0])) {
							query += "! > ?";
							kal.add(LEGIT_PARAM_NAMES_BOOKINGS[1]);
						} else {
							query += "! < ?";
							kal.add(LEGIT_PARAM_NAMES_BOOKINGS[0]);
						}
						val.add(map.get(key)[0]);
					}
				}
			}
		}

		if (atLeastOneKeyFoundInSecondArray)
			query += QUERY_MIDDLE_2;

		boolean atLeastOneKeyFoundInThirdArray = false;

		for (String key : map.keySet()) {
			for (String param : LEGIT_PARAM_NAMES_ROOMS) {
				if (key.equals(param) && !map.get(key)[0].isEmpty()) {
					atLeastOneKeyFoundInThirdArray = true;
					break;
				}
			}
			if (atLeastOneKeyFoundInThirdArray)
				break;
		}

		if ((atLeastOneKeyFoundInFirstArray || atLeastOneKeyFoundInSecondArray) && atLeastOneKeyFoundInThirdArray) // //
			query += " and ";
		else if (atLeastOneKeyFoundInThirdArray)
			query += " where ";

		firstParamEntered = false;

		if (atLeastOneKeyFoundInThirdArray) {
			for (String key : map.keySet()) {
				for (String param : LEGIT_PARAM_NAMES_ROOMS) {
					if (key.equals(param) && !map.get(key)[0].isEmpty()) {
						if (firstParamEntered)
							query += " and ";
						else
							firstParamEntered = true;
						if (key.equals(LEGIT_PARAM_NAMES_ROOMS[3]) || key.equals(LEGIT_PARAM_NAMES_ROOMS[4]) || key.equals(LEGIT_PARAM_NAMES_ROOMS[5]))
							query += "! >= ?";
						else if (key.equals(LEGIT_PARAM_NAMES_ROOMS[6]))
							query += "! <= ?";
						else
							query += QUERY_PARAM;
						if (key.equals(LEGIT_PARAM_NAMES_ROOMS[5]) || key.equals(LEGIT_PARAM_NAMES_ROOMS[6]))
							kal.add("price");
						else
							kal.add(key);
						if (key.equals(LEGIT_PARAM_NAMES_ROOMS[7]))
							val.add(map.get(key)[0].equals("true") ? "1" : "0");
						else
							val.add(map.get(key)[0]);
					}
				}
			}
		}

		query += END_QUERY;

		for (String s : kal)
			query = query.replaceFirst("!", s);

		if (LOG_QUERY) {
			System.out.println(query);
			for (String s : val)
				System.out.println(s);
		}

		String[] sa = new String[val.size()];
		return get(query, val.toArray(sa));
	}

	// PRIVATE METHODS :

	static private final Room[] get(String query, String... stringParams) {
		/*
		 * Retourne un array de Rooms, selon la requête
		 * 
		 * String query : une requête sql qui selectionne toutes les colonnes de la table
		 * 
		 * String... stringParams : les Strings qui remplacent les paramètres ? de la requête (optionnels)
		 * 
		 */

		ArrayList<Room> rooms = new ArrayList<Room>();
		try {
			PreparedStatement ps = DBConnexion.getPs(query);
			if (stringParams != null)
				for (int i = 0; i < stringParams.length; i++)
					ps.setString(i + 1, stringParams[i]);
			ps.executeQuery();
			ResultSet rs = ps.getResultSet();
			while (rs.next())
				rooms.add(new Room(rs.getInt(1), rs.getString(2), rs.getInt(3), rs.getInt(4), rs.getInt(5), rs.getInt(6), rs.getFloat(7), rs.getBoolean(8)));
		} catch (SQLException e) {
			e.printStackTrace();
		}
		DBConnexion.close();
		Room[] ra = new Room[rooms.size()];
		return rooms.toArray(ra);
	}

}
