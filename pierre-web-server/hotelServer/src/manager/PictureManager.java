package manager;

import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;

import controller.Resources;
import entity.Picture;
import service.DBConnexion;

public class PictureManager {

	static private final String PICTURES_PATH_BEGINNING = Resources.SRV_PROTOCOL + Resources.SRV_IP_ADDR + Resources.SRV_IP_PORT_SEPARATOR + Resources.SRV_LISTENING_PORT + Resources.SRV_FOLDER_SEPARATOR + Resources.SRV_FOLDER + Resources.SRV_FOLDER_SEPARATOR + Resources.SRV_PICTURE_FOLDER + Resources.SRV_FOLDER_SEPARATOR;

	static private final String TABLE_NAME = "pictures";

	static private final String OP_SELECT = Resources.OP_SELECT;
	static private final String OP_EQUALS = Resources.OP_EQUALS;
	static private final String OP_REPLACE = Resources.OP_REPLACE;

	static private final String CLAUSE_FROM = Resources.CLAUSE_FROM;
	static private final String CLAUSE_WHERE = Resources.CLAUSE_WHERE;

	static private final String COL_ALL = Resources.COL_ALL;
	static private final String COL_ID = Resources.COL_ID;
	static private final String COL_ID_ROOM = "idRoom";

	static private final String END_QUERY = Resources.END_QUERY;

	static private final String QUERY_GET_ALL = OP_SELECT + " " + COL_ALL + " " + CLAUSE_FROM + " " + TABLE_NAME + END_QUERY;

	static public Picture[] getAll() {
		return get(QUERY_GET_ALL);
	}

	static private final String QUERY_GET_BY_ID = OP_SELECT + " " + COL_ALL + " " + CLAUSE_FROM + " " + TABLE_NAME + " " + CLAUSE_WHERE + " " + COL_ID + " " + OP_EQUALS + " " + OP_REPLACE + END_QUERY;

	static public final Picture[] getById(String id) {
		return get(QUERY_GET_BY_ID, id);
	}

	static private final String QUERY_GET_BY_ID_ROOM = OP_SELECT + " " + COL_ALL + " " + CLAUSE_FROM + " " + TABLE_NAME + " " + CLAUSE_WHERE + " " + COL_ID_ROOM + " " + OP_EQUALS + " " + OP_REPLACE + END_QUERY;

	static public final Picture[] getByIdRoom(String idRoom) {
		return get(QUERY_GET_BY_ID_ROOM, idRoom);
	}

	// PRIVATE METHODS :

	static private final Picture[] get(String query, String... stringParams) {
		ArrayList<Picture> pictures = new ArrayList<Picture>();
		try {
			PreparedStatement ps = DBConnexion.getPs(query);
			if (stringParams != null)
				for (int i = 0; i < stringParams.length; i++)
					ps.setString(i + 1, stringParams[i]);
			ps.executeQuery();
			ResultSet rs = ps.getResultSet();
			while (rs.next())
				pictures.add(new Picture(rs.getInt(1), rs.getInt(2), PICTURES_PATH_BEGINNING + rs.getString(3)));
		} catch (SQLException e) {
			e.printStackTrace();
		}
		DBConnexion.close();
		Picture[] pa = new Picture[pictures.size()];
		return pictures.toArray(pa);
	}

}
