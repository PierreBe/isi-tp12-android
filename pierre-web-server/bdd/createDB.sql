drop database if exists hotels;
create database hotels;
use hotels;

create table hotels(
	id int auto_increment primary key,
    name varchar(255) not null,
    phoneNumber varchar(255) not null,
    email varchar(255) not null,
    addrL1 varchar(255) not null,
    addrL2 varchar(255) not null,
    zip varchar(255) not null,
    city varchar(255) not null,
    state varchar(255) not null,
    country varchar(255) not null,
    parkNumber int not null,
    breakfast bool not null,
    lunch bool not null,
    dinner bool not null
);

create table views(
	id int auto_increment primary key,
    name varchar(255)
);

create table rooms(
	id int auto_increment primary key,
    description varchar(255),
    idHotel int not null,
    foreign key (idHotel) references hotels (id),
    idView int not null,
    foreign key (idView) references views (id),
    singleBedNumber int not null,
    doubleBedNumber int not null,
    price float not null,
    smoker bool not null
);

create table pictures(
	id int auto_increment primary key,
    idRoom int not null,
    foreign key (idRoom) references rooms (id),
    url varchar(255) not null
);

create table clients(
	id int auto_increment primary key,
    firstName varchar(255) not null,
    lastName varchar(255) not null,
    email varchar(255) not null,
    pwdHash varchar(255) not null
);

create table purposes(
	id int auto_increment primary key,
    name varchar(255) not null
);

create table bookings(
	id int auto_increment primary key,
    idClient int not null,
    foreign key (idClient) references clients (id),
    idHotel int not null,
    foreign key (idHotel) references hotels (id),
    idRoom int not null,
    foreign key (idRoom) references rooms (id),
    idPurpose int not null,
    foreign key (idPurpose) references purposes (id),
    fromDate date not null,
    toDate date not null,
    breakfastNumber int not null,
    lunchNumber int not null,
    dinnerNumber int not null
);