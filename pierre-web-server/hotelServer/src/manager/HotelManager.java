package manager;

import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.Arrays;

import controller.Resources;
import entity.Hotel;
import service.DBConnexion;

public class HotelManager {

	static private final String TABLE_NAME = "hotels";

	static private final String OP_SELECT = Resources.OP_SELECT;
	static private final String OP_EQUALS = Resources.OP_EQUALS;
	static private final String OP_REPLACE = Resources.OP_REPLACE;

	static private final String CLAUSE_FROM = Resources.CLAUSE_FROM;
	static private final String CLAUSE_WHERE = Resources.CLAUSE_WHERE;
	static private final String CLAUSE_GROUP = Resources.CLAUSE_GROUP;

	static private final String COL_ALL = Resources.COL_ALL;
	static private final String COL_ID = Resources.COL_ID;
	static private final String COL_CITY = Resources.COL_CITY;
	static private final String COL_STATE = Resources.COL_STATE;
	static private final String COL_COUNTRY = Resources.COL_COUNTRY;

	static private final String END_QUERY = Resources.END_QUERY;

	static private final String QUERY_GET_ALL = OP_SELECT + " " + COL_ALL + " " + CLAUSE_FROM + " " + TABLE_NAME + END_QUERY;

	static public Hotel[] getAll() {
		return get(QUERY_GET_ALL);
	}

	static private final String QUERY_GET_BY_ID = OP_SELECT + " " + COL_ALL + " " + CLAUSE_FROM + " " + TABLE_NAME + " " + CLAUSE_WHERE + " " + COL_ID + " " + OP_EQUALS + " " + OP_REPLACE + END_QUERY;

	static public final Hotel[] getById(String id) {
		return get(QUERY_GET_BY_ID, id);
	}

	static private final String QUERY_GET_BY_CITY = OP_SELECT + " " + COL_ALL + " " + CLAUSE_FROM + " " + TABLE_NAME + " " + CLAUSE_WHERE + " " + COL_CITY + " " + OP_EQUALS + " " + OP_REPLACE + END_QUERY;

	static public final Hotel[] getByCity(String city) {
		return get(QUERY_GET_BY_CITY, city);
	}

	static private final String QUERY_GET_BY_STATE = OP_SELECT + " " + COL_ALL + " " + CLAUSE_FROM + " " + TABLE_NAME + " " + CLAUSE_WHERE + " " + COL_STATE + " " + OP_EQUALS + " " + OP_REPLACE + END_QUERY;

	static public final Hotel[] getByState(String state) {
		return get(QUERY_GET_BY_STATE, state);
	}

	static private final String QUERY_GET_BY_COUNTRY = OP_SELECT + " " + COL_ALL + " " + CLAUSE_FROM + " " + TABLE_NAME + " " + CLAUSE_WHERE + " " + COL_COUNTRY + " " + OP_EQUALS + " " + OP_REPLACE + END_QUERY;

	static public final Hotel[] getByCountry(String country) {
		return get(QUERY_GET_BY_COUNTRY, country);
	}

	static private final String QUERY_GET_CITIES = OP_SELECT + " " + COL_CITY + " " + CLAUSE_FROM + " " + TABLE_NAME + " " + CLAUSE_GROUP + " " + COL_CITY + END_QUERY;

	static public final String[] getCities() {
		return getColStringArray(QUERY_GET_CITIES);
	}

	static private final String QUERY_GET_CITIES_BY_STATE = OP_SELECT + " " + COL_CITY + " " + CLAUSE_FROM + " " + TABLE_NAME + " " + CLAUSE_WHERE + " " + COL_STATE + OP_EQUALS + " " + OP_REPLACE + " " + CLAUSE_GROUP + " " + COL_CITY + END_QUERY;

	static public final String[] getCitiesByState(String state) {
		return getColStringArray(QUERY_GET_CITIES_BY_STATE, state);
	}

	static private final String QUERY_GET_CITIES_BY_COUNTRY = OP_SELECT + " " + COL_CITY + " " + CLAUSE_FROM + " " + TABLE_NAME + " " + CLAUSE_WHERE + " " + COL_COUNTRY + OP_EQUALS + " " + OP_REPLACE + " " + CLAUSE_GROUP + " " + COL_CITY + END_QUERY;

	static public final String[] getCitiesByCountry(String country) {
		return getColStringArray(QUERY_GET_CITIES_BY_COUNTRY, country);
	}

	static private final String QUERY_GET_STATES = OP_SELECT + " " + COL_STATE + " " + CLAUSE_FROM + " " + TABLE_NAME + " " + CLAUSE_GROUP + " " + COL_STATE + END_QUERY;

	static public final String[] getStates() {
		return getColStringArray(QUERY_GET_STATES);
	}

	static private final String QUERY_GET_STATES_BY_COUNTRY = OP_SELECT + " " + COL_STATE + " " + CLAUSE_FROM + " " + TABLE_NAME + " " + CLAUSE_WHERE + " " + COL_COUNTRY + OP_EQUALS + " " + OP_REPLACE + " " + CLAUSE_GROUP + " " + COL_STATE + END_QUERY;

	static public final String[] getStatesByCountry(String country) {
		return getColStringArray(QUERY_GET_STATES_BY_COUNTRY, country);
	}

	static private final String QUERY_GET_COUNTRIES = OP_SELECT + " " + COL_COUNTRY + " " + CLAUSE_FROM + " " + TABLE_NAME + " " + CLAUSE_GROUP + " " + COL_COUNTRY + END_QUERY;

	static public String[] getCountries() {
		return getColStringArray(QUERY_GET_COUNTRIES);
	}

	// PRIVATE METHODS :

	// static public HashMap<Integer, Hotel> getAll() { // SparseArray ?
	// HashMap<Integer, Hotel> hotels = new HashMap<Integer, Hotel>();
	// static public ArrayList<Hotel> getAll() {
	static private final Hotel[] get(String query, String... stringParams) {
		/*
		 * Retourne un array d'Hotels, selon la requête
		 * 
		 * String query : une requête sql qui selectionne toutes les colonnes de la table
		 * 
		 * String... stringParams : les Strings qui remplacent les paramètres ? de la requête (optionnels)
		 * 
		 */
		ArrayList<Hotel> hotels = new ArrayList<Hotel>();
		try {
			PreparedStatement ps = DBConnexion.getPs(query);
			if (stringParams != null)
				for (int i = 0; i < stringParams.length; i++)
					ps.setString(i + 1, stringParams[i]);
			ps.executeQuery();
			ResultSet rs = ps.getResultSet();
			while (rs.next())
				// hotels.put(rs.getInt(1), new Hotel(rs.getInt(1), rs.getString(2), rs.getString(3), rs.getString(4), rs.getString(5), rs.getString(6), rs.getString(7), rs.getString(8), rs.getString(9), rs.getString(10), rs.getInt(11), rs.getBoolean(12), rs.getBoolean(13), rs.getBoolean(14)));
				hotels.add(new Hotel(rs.getInt(1), rs.getString(2), rs.getString(3), rs.getString(4), rs.getString(5), rs.getString(6), rs.getString(7), rs.getString(8), rs.getString(9), rs.getString(10), rs.getInt(11), rs.getBoolean(12), rs.getBoolean(13), rs.getBoolean(14)));
		} catch (SQLException e) {
			e.printStackTrace();
		}
		DBConnexion.close();
		// return hotels;
		Hotel[] ha = new Hotel[hotels.size()];
		return hotels.toArray(ha);
	}

	static private final String[] getColStringArray(String query, String... stringParams) {
		/*
		 * Retourne un array de Strings classées dans l'ordre alphabétique
		 * 
		 * String query : une requête sql qui ne selectionne qu'une colonne de type varchar(String)
		 * 
		 * String... stringParams : les Strings qui remplacent les paramètres ? de la requête (optionnels)
		 * 
		 */
		ArrayList<String> al = new ArrayList<String>();
		try {
			PreparedStatement ps = DBConnexion.getPs(query);
			if (stringParams != null)
				for (int i = 0; i < stringParams.length; i++)
					ps.setString(i + 1, stringParams[i]);
			ps.executeQuery();
			ResultSet rs = ps.getResultSet();
			while (rs.next())
				al.add(rs.getString(1));
		} catch (SQLException e) {
			e.printStackTrace();
		}
		DBConnexion.close();
		String[] a = new String[al.size()];
		al.toArray(a);
		Arrays.sort(a);
		return a;
	}

}
