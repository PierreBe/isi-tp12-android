package controller;

public class Resources {

	// SÉRIALISATION/DÉSÉRIALISATION :

	static public final String SEPARATOR_ARRAY = ";";
	static public final String SEPARATOR_PARAMETER = "/";

	// SYNTAXE SQL :

	static public final String OP_SELECT = "select";
	static public final String OP_EQUALS = "=";
	static public final String OP_REPLACE = "?";

	static public final String CLAUSE_FROM = "from";
	static public final String CLAUSE_WHERE = "where";
	static public final String CLAUSE_GROUP = "group by";

	static public final String COL_ALL = "*";
	static public final String COL_ID = "id";
	static public final String COL_CITY = "city";
	static public final String COL_STATE = "state";
	static public final String COL_COUNTRY = "country";

	static public final String END_QUERY = ";";

	// PARAMÈTRES SERVEUR :

	static public final String SRV_PROTOCOL = "http://";
	static public final String SRV_IP_ADDR = "70.53.207.187";
	static public final String SRV_IP_PORT_SEPARATOR = ":";
	static public final String SRV_LISTENING_PORT = "9876";
	static public final String SRV_FOLDER_SEPARATOR = "/";
	static public final String SRV_FOLDER = "relax-inn-server";
	static public final String SRV_PICTURE_FOLDER = "room-pictures";

}
