package controller;

import java.io.IOException;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import entity.Room;
import manager.RoomManager;

@WebServlet("/rooms")
public class RoomController extends HttpServlet {

	private static final long serialVersionUID = 1L;

	public RoomController() {
		super();
	}

	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {

		// Room[] rooms;

		/*if (request.getParameter("id") != null)
			rooms = RoomManager.getById(request.getParameter("id"));
		else if (request.getParameter("city") != null)
			rooms = RoomManager.getByCity(request.getParameter("city"));
		else if (request.getParameter("state") != null)
			rooms = RoomManager.getByState(request.getParameter("state"));
		else if (request.getParameter("country") != null)
			rooms = RoomManager.getByCountry(request.getParameter("country"));
		else if (request.getParameter("idHotel") != null)
			rooms = RoomManager.getByIdHotel(request.getParameter("idHotel"));
		else if (request.getParameter("idView") != null)
			rooms = RoomManager.getByIdView(request.getParameter("idView"));
		else if (request.getParameter("singleBedNumber") != null && request.getParameter("doubleBedNumber") != null)
			rooms = RoomManager.getByBedNumber(request.getParameter("singleBedNumber"), request.getParameter("doubleBedNumber"));
		else if (request.getParameter("priceFloor") != null && request.getParameter("priceCeiling") != null)
			rooms = RoomManager.getByPrice(request.getParameter("priceFloor"), request.getParameter("priceCeiling"));
		else if (request.getParameter("smoker") != null)
			rooms = RoomManager.getBySmoker(request.getParameter("smoker"));
		else
			rooms = RoomManager.getAll();*/

		// rooms = RoomManager.getByAnyParameter(request.getParameterMap());

		// response.getWriter().append(Room.serialize(rooms));
		response.getWriter().append(Room.serialize(RoomManager.getByAnyParameter(request.getParameterMap())));

	}

	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		doGet(request, response);
	}

}
