package service;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.SQLException;

public class DBConnexion {

	private final static String urlConnection = "jdbc:mysql://localhost:3306/hotels?serverTimezone=UTC";
	private final static String login = "root";
	private final static String pwd = "toor";
	private static Connection connexion;

	public static PreparedStatement getPs(String query) {
		PreparedStatement ps = null;
		try {
			if (connexion == null || connexion.isClosed()) {
				Class.forName("com.mysql.cj.jdbc.Driver");
				connexion = DriverManager.getConnection(urlConnection, login, pwd);
				ps = connexion.prepareStatement(query);
			}
		} catch (SQLException e) {
			e.printStackTrace();
		} catch (ClassNotFoundException e) {
			e.printStackTrace();
		}
		return ps;
	}

	public static void close() {
		try {
			connexion.close();
		} catch (SQLException e) {
			e.printStackTrace();
		}
	}

}
