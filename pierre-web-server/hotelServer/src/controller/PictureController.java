package controller;

import java.io.IOException;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import entity.Picture;
import manager.PictureManager;

@WebServlet("/pictures")
public class PictureController extends HttpServlet {

	private static final long serialVersionUID = 1L;

	public PictureController() {
		super();
	}

	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {

		Picture[] pictures;

		if (request.getParameter("id") != null && !request.getParameter("id").isEmpty())
			pictures = PictureManager.getById(request.getParameter("id"));
		else if (request.getParameter("idRoom") != null && !request.getParameter("idRoom").isEmpty())
			pictures = PictureManager.getByIdRoom(request.getParameter("idRoom"));
		else
			pictures = PictureManager.getAll();

		response.getWriter().append(Picture.serialize(pictures));

	}

	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		doGet(request, response);
	}

}
