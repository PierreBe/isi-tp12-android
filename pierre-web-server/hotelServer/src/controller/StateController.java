package controller;

import java.io.IOException;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import manager.HotelManager;

@WebServlet("/states")
public class StateController extends HttpServlet {

	private static final long serialVersionUID = 1L;

	public StateController() {
		super();
	}

	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {

		String[] states;

		if (request.getParameter("country") != null && !request.getParameter("country").isEmpty())
			states = HotelManager.getStatesByCountry(request.getParameter("country"));
		else
			states = HotelManager.getStates();

		response.getWriter().append(String.join(Resources.SEPARATOR_ARRAY, states));

	}

	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		doGet(request, response);
	}

}
