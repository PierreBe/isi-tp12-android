package manager;

import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.Map;

import controller.Resources;
import entity.Booking;
import service.DBConnexion;

public class BookingManager {

	static private final boolean LOG_QUERY = false;

	static private final String TABLE_NAME = "bookings";

	static private final String OP_SELECT = Resources.OP_SELECT;
	static private final String OP_EQUALS = Resources.OP_EQUALS;
	static private final String OP_REPLACE = Resources.OP_REPLACE;

	static private final String CLAUSE_FROM = Resources.CLAUSE_FROM;
	static private final String CLAUSE_WHERE = Resources.CLAUSE_WHERE;

	static private final String COL_ALL = Resources.COL_ALL;
	static private final String COL_ID = Resources.COL_ID;
	static private final String COL_CITY = Resources.COL_CITY;
	static private final String COL_STATE = Resources.COL_STATE;
	static private final String COL_COUNTRY = Resources.COL_COUNTRY;

	static private final String END_QUERY = Resources.END_QUERY;

	static private final String QUERY_BEGIN = OP_SELECT + " " + COL_ALL + " " + CLAUSE_FROM + " " + TABLE_NAME;

	static private final String QUERY_PARAM = "! = ?";

	static private final String QUERY_MIDDLE_1 = "idHotel in (select id from hotels where ";
	static private final String QUERY_MIDDLE_2 = ")";

	static private final String[] LEGIT_PARAM_NAMES_BOOKINGS = { "id", "idClient", "idHotel", "idRoom", "idPurpose", "fromDate", "toDate" };
	static private final String[] LEGIT_PARAM_NAMES_HOTELS = { "city", "state", "country" };

	static public final Booking[] getByAnyParameter(Map<String, String[]> map) {

		String query = QUERY_BEGIN;
		ArrayList<String> kal = new ArrayList<String>();
		ArrayList<String> val = new ArrayList<String>();

		boolean firstParamEntered = false;

		for (String key : map.keySet()) {
			for (String param : LEGIT_PARAM_NAMES_BOOKINGS) {
				if (key.equals(param) && !map.get(key)[0].isEmpty()) {
					if (firstParamEntered)
						query += " and ";
					else {
						query += " where ";
						firstParamEntered = true;
					}
					if (key.equals(LEGIT_PARAM_NAMES_BOOKINGS[5])) {
						query += "! > ?";
						kal.add(LEGIT_PARAM_NAMES_BOOKINGS[6]);
					} else if (key.equals(LEGIT_PARAM_NAMES_BOOKINGS[6])) {
						query += "! < ?";
						kal.add(LEGIT_PARAM_NAMES_BOOKINGS[5]);
					} else {
						query += QUERY_PARAM;
						kal.add(key);
					}
					val.add(map.get(key)[0]);
				}
			}
		}

		boolean firstParamFromSecondArrayEntered = false;

		for (String key : map.keySet()) {
			for (String param : LEGIT_PARAM_NAMES_HOTELS) {
				if (key.equals(param) && !map.get(key)[0].isEmpty()) {
					if (firstParamFromSecondArrayEntered)
						query += " and ";
					else {
						if (firstParamEntered)
							query += " and " + QUERY_MIDDLE_1;
						else
							query += " where " + QUERY_MIDDLE_1;
						firstParamFromSecondArrayEntered = true;
					}
					query += QUERY_PARAM;
					kal.add(key);
					val.add(map.get(key)[0]);
				}
			}
		}

		if (firstParamFromSecondArrayEntered)
			query += QUERY_MIDDLE_2;

		query += END_QUERY;

		for (String s : kal)
			query = query.replaceFirst("!", s);

		if (LOG_QUERY) {
			System.out.println(query);
			for (String s : val)
				System.out.println(s);
		}

		String[] sa = new String[val.size()];
		return get(query, val.toArray(sa));
	}

	static private final String INSERT_NEW_BOOKING = "insert into bookings (idClient,idHotel,idRoom,idPurpose,fromDate,toDate,breakfastNumber,lunchNumber,dinnerNumber) values (?,?,?,?,?,?,?,?,?);";

	static public final Booking create(String serializedBooking) {
		Booking b = Booking.deserializeItem(serializedBooking);

		// Vérifier que la chambre n'est pas déjà réservée ?

		if (b.getId() != -1 || update(INSERT_NEW_BOOKING, "" + b.getIdClient(), "" + b.getIdHotel(), "" + b.getIdRoom(), "" + b.getIdPurpose(), b.getFromDate().toString(), b.getToDate().toString(), "" + b.getBreakfastNumber(), "" + b.getLunchNumber(), "" + b.getDinnerNumber()) != 1)
			b = null;
		return b;
	}

	// PRIVATE METHODS :

	static private final Booking[] get(String query, String... stringParams) {
		ArrayList<Booking> bookings = new ArrayList<Booking>();
		try {
			PreparedStatement ps = DBConnexion.getPs(query);
			if (stringParams != null)
				for (int i = 0; i < stringParams.length; i++)
					ps.setString(i + 1, stringParams[i]);
			ps.executeQuery();
			ResultSet rs = ps.getResultSet();
			while (rs.next())
				bookings.add(new Booking(rs.getInt(1), rs.getInt(2), rs.getInt(3), rs.getInt(4), rs.getInt(5), rs.getDate(6), rs.getDate(7), rs.getInt(8), rs.getInt(9), rs.getInt(10)));
		} catch (SQLException e) {
			e.printStackTrace();
		}
		DBConnexion.close();
		Booking[] ba = new Booking[bookings.size()];
		return bookings.toArray(ba);
	}

	static private final int update(String query, String... stringParams) {
		int r = 0;
		try {
			PreparedStatement ps = DBConnexion.getPs(query);
			if (stringParams != null)
				for (int i = 0; i < stringParams.length; i++)
					ps.setString(i + 1, stringParams[i]);
			r = ps.executeUpdate();
		} catch (SQLException e) {
			e.printStackTrace();
		}
		DBConnexion.close();
		return r;
	}

}
