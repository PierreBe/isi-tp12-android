package controller;

import java.io.IOException;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import entity.View;
import manager.ViewManager;

@WebServlet("/views")
public class ViewController extends HttpServlet {

	private static final long serialVersionUID = 1L;

	public ViewController() {
		super();
	}

	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {

		View[] views;

		if (request.getParameter("id") != null && !request.getParameter("id").isEmpty())
			views = ViewManager.getById(request.getParameter("id"));
		else
			views = ViewManager.getAll();

		response.getWriter().append(View.serialize(views));

	}

	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		doGet(request, response);
	}

}
