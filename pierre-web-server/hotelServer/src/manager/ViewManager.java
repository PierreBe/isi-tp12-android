package manager;

import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;

import controller.Resources;
import entity.View;
import service.DBConnexion;

public class ViewManager {

	static private final String TABLE_NAME = "views";

	static private final String OP_SELECT = Resources.OP_SELECT;
	static private final String OP_EQUALS = Resources.OP_EQUALS;
	static private final String OP_REPLACE = Resources.OP_REPLACE;

	static private final String CLAUSE_FROM = Resources.CLAUSE_FROM;
	static private final String CLAUSE_WHERE = Resources.CLAUSE_WHERE;

	static private final String COL_ALL = Resources.COL_ALL;
	static private final String COL_ID = Resources.COL_ID;

	static private final String END_QUERY = Resources.END_QUERY;

	static private final String QUERY_GET_ALL = OP_SELECT + " " + COL_ALL + " " + CLAUSE_FROM + " " + TABLE_NAME + END_QUERY;

	static public View[] getAll() {
		return get(QUERY_GET_ALL);
	}

	static private final String QUERY_GET_BY_ID = OP_SELECT + " " + COL_ALL + " " + CLAUSE_FROM + " " + TABLE_NAME + " " + CLAUSE_WHERE + " " + COL_ID + " " + OP_EQUALS + " " + OP_REPLACE + END_QUERY;

	static public final View[] getById(String id) {
		return get(QUERY_GET_BY_ID, id);
	}

	// PRIVATE METHODS :

	static private final View[] get(String query, String... stringParams) {
		ArrayList<View> views = new ArrayList<View>();
		try {
			PreparedStatement ps = DBConnexion.getPs(query);
			if (stringParams != null)
				for (int i = 0; i < stringParams.length; i++)
					ps.setString(i + 1, stringParams[i]);
			ps.executeQuery();
			ResultSet rs = ps.getResultSet();
			while (rs.next())
				views.add(new View(rs.getInt(1), rs.getString(2)));
		} catch (SQLException e) {
			e.printStackTrace();
		}
		DBConnexion.close();
		View[] va = new View[views.size()];
		return views.toArray(va);
	}

}
