-- ! You should run createDB.sql before executing this script !

use hotels;

-- insert into hotels (name,phoneNumber,email,addrL1,addrL2,zip,city,state,country,parkNumber,breakfast,lunch,dinner) 
-- 	values ("name",
-- 		"phoneNumber",
--         "email",
--         "addrL1",
--         "addrL2",
--         "zip",
--         "city",
--         "state",
--         "country",
--         parkNumber,
-- 		breakfast,
-- 		lunch,
-- 		dinner);

insert into hotels (name,phoneNumber,email,addrL1,addrL2,zip,city,state,country,parkNumber,breakfast,lunch,dinner) 
	values ("Hotel Dorion",
		"(514) 523-2427",
        "info@hoteldorion.ca",
        "1477 rue Dorion",
        "",
        "H2K 4A4",
        "Montréal",
        "Québec",
        "Canada",
        5,
        true,
        false,
        false);

insert into hotels (name,phoneNumber,email,addrL1,addrL2,zip,city,state,country,parkNumber,breakfast,lunch,dinner) 
	values ("Hotel Sainte-Catherine",
		"(514) 527-4440",
        "info@stecatherinehotel.com",
        "1674 rue Sainte-Catherine Est",
        "",
        "H2L 2J4",
        "Montréal",
        "Québec",
        "Canada",
        0,
        true,
        false,
        true);

insert into hotels (name,phoneNumber,email,addrL1,addrL2,zip,city,state,country,parkNumber,breakfast,lunch,dinner) 
	values ("Best Western PLUS Centre-ville Québec",
		"(418) 649-1919",
        "",
        "330 de la Couronne",
        "",
        "G1K 6E6",
        "Québec",
        "Québec",
        "Canada",
        15,
        true,
        true,
        true);

insert into hotels (name,phoneNumber,email,addrL1,addrL2,zip,city,state,country,parkNumber,breakfast,lunch,dinner) 
	values ("Ramada Limited Vancouver Downtown",
		"(604) 488-1088",
        "",
        "435 W Pender St",
        "",
        "V6B 1V2",
        "Vancouver",
        "British Columbia",
        "Canada",
        10,
        true,
        false,
        false);

insert into hotels (name,phoneNumber,email,addrL1,addrL2,zip,city,state,country,parkNumber,breakfast,lunch,dinner) 
	values ("Château Laurier",
		"(613) 241-1414",
        "",
        "1 Rideau St",
        "",
        "K1N 8S7",
        "Ottawa",
        "Ontario",
        "Canada",
        20,
        true,
        true,
        true);

insert into hotels (name,phoneNumber,email,addrL1,addrL2,zip,city,state,country,parkNumber,breakfast,lunch,dinner) 
	values ("Holiday Inn Express & Suites Ottawa West - Nepean",
		"(613) 690-0100",
        "",
        "2055 Robertson Rd",
        "",
        "K2H 5Y9",
        "Ottawa",
        "Ontario",
        "Canada",
        50,
        true,
        false,
        false);

insert into hotels (name,phoneNumber,email,addrL1,addrL2,zip,city,state,country,parkNumber,breakfast,lunch,dinner) 
	values ("Holiday Inn Paris - Gare de l'Est",
		"+33 1 55 26 05 05",
        "",
        "5 Rue du 8 Mai 1945",
        "",
        "75010",
        "Paris",
        "",
        "France",
        0,
        true,
        false,
        true);

insert into hotels (name,phoneNumber,email,addrL1,addrL2,zip,city,state,country,parkNumber,breakfast,lunch,dinner) 
	values ("Hôtel restaurant Lunotel Saint-Lo",
		"+33 2 33 56 56 56",
        "",
        "130 Rue de la Liberté,",
        "",
        "50000",
        "Saint-Lô",
        "",
        "France",
        50,
        true,
        true,
        true);
        
-- insert into views (name)
-- 	values("name");
        
insert into views (name)
	values("parking");
        
insert into views (name)
	values("rue");
        
insert into views (name)
	values("place");
        
insert into views (name)
	values("mer");
        
insert into views (name)
	values("montagne");
        
insert into views (name)
	values("fleuve");
    
-- insert into rooms (description,idHotel,idView,singleBedNumber,doubleBedNumber,price,smoker)
-- 	values ("description",
-- 		idHotel,
--         idView,
--         singleBedNumber,
--         doubleBedNumber,
--         price,
--         smoker,
--         breakfast,
--         lunch,
--         dinner);
    
insert into rooms (description,idHotel,idView,singleBedNumber,doubleBedNumber,price,smoker)
	values ("chambre 2 lits simples au calme, fumeur",
		1,
        1,
        2,
        0,
        49.99,
        true);
    
insert into rooms (description,idHotel,idView,singleBedNumber,doubleBedNumber,price,smoker)
	values ("chambre 1 lit double au calme",
		1,
        1,
        0,
        1,
        49.99,
        false);
    
insert into rooms (description,idHotel,idView,singleBedNumber,doubleBedNumber,price,smoker)
	values ("chambre familiale",
		1,
        1,
        2,
        1,
        74.99,
        false);
    
insert into rooms (description,idHotel,idView,singleBedNumber,doubleBedNumber,price,smoker)
	values ("chambre amoureux",
		2,
        2,
        0,
        1,
        63.49,
        true);
    
insert into rooms (description,idHotel,idView,singleBedNumber,doubleBedNumber,price,smoker)
	values ("chambre swingers",
		2,
        2,
        0,
        2,
        100,
        true);
    
insert into rooms (description,idHotel,idView,singleBedNumber,doubleBedNumber,price,smoker)
	values ("chambre deux lits simples",
		2,
        2,
        2,
        0,
        49.99,
        true);
    
insert into rooms (description,idHotel,idView,singleBedNumber,doubleBedNumber,price,smoker)
	values ("chambre deux lits simples, un lit double",
		3,
        3,
        2,
        1,
        74.99,
        false);
    
insert into rooms (description,idHotel,idView,singleBedNumber,doubleBedNumber,price,smoker)
	values ("chambre lit double",
		3,
        2,
        0,
        1,
        49.99,
        true);
    
insert into rooms (description,idHotel,idView,singleBedNumber,doubleBedNumber,price,smoker)
	values ("chambre avec belle vue sur la montagne",
		5,
        5,
        2,
        0,
        80.5,
        false);
        
-- insert into pictures (idRoom,url)
-- 	values (idRoom,
-- 		"url");
        
insert into pictures (idRoom,url)
	values (1,
		"hotelTemplate1/1doubleBedSample.jpg");
        
insert into pictures (idRoom,url)
	values (2,
		"hotelTemplate1/2singleBedSample2.jpg");
        
insert into pictures (idRoom,url)
	values (3,
		"hotelTemplate1/2doubleBedSample.jpg");
        
insert into pictures (idRoom,url)
	values (4,
		"hotelTemplate1/2singleBedSample.jpg");
        
insert into pictures (idRoom,url)
	values (4,
		"hotelTemplate2/bathroomSample.jpg");
        
insert into pictures (idRoom,url)
	values (7,
		"hotelTemplate2/bathroomSample.jpg");
        
insert into pictures (idRoom,url)
	values (8,
		"hotelTemplate1/2doubleBedSample2.jpg");
        
insert into pictures (idRoom,url)
	values (9,
		"hotelTemplate2/2singleBedSample2.jpg");
        
insert into pictures (idRoom,url)
	values (9,
		"hotelTemplate2/bathroomSample4.jpg");

-- insert into clients (firstName,lastName,email,pwdHash)
-- 	values ("firstName",
-- 		"lastName",
-- 		"email",
-- 		"pwdHash");

insert into clients (firstName,lastName,email,pwdHash)
	values ("Pierre",
		"Bodineau",
		"pbodinea@isi-mtl.com",
		"pbodinea");

insert into clients (firstName,lastName,email,pwdHash)
	values ("César",
		"Trevino",
		"ctrevino@isi-mtl.com",
		"ctrevino");

insert into clients (firstName,lastName,email,pwdHash)
	values ("Francis",
		"Bibeau",
		"fbibeau@isi-mtl.com",
		"fbibeau");

insert into clients (firstName,lastName,email,pwdHash)
	values ("Ariane",
		"Teuboh",
		"ateuboh@isi-mtl.com",
		"ateuboh");

insert into clients (firstName,lastName,email,pwdHash)
	values ("Jean-François",
		"Lidou",
		"jlidou@isi-mtl.com",
		"jlidou");

-- insert into purposes (name)
-- 	values ("name");

insert into purposes (name)
	values ("nd");

insert into purposes (name)
	values ("loisir");

insert into purposes (name)
	values ("business");

-- insert into bookings (idClient,idHotel,idRoom,idPurpose,fromDate,toDate,breakfastNumber,lunchNumber,dinnerNumber)
-- 	values (idClient,
-- 		idHotel,
-- 		idRoom,
-- 		idPurpose,
-- 		'fromDate',
-- 		'toDate',
-- 		breakfastNumber,
-- 		lunchNumber,
-- 		dinnerNumber);

insert into bookings (idClient,idHotel,idRoom,idPurpose,fromDate,toDate,breakfastNumber,lunchNumber,dinnerNumber)
	values (1,
		1,
		1,
		1,
		'2019-10-5',
		'2019-10-10',
		5,
		0,
		2);

insert into bookings (idClient,idHotel,idRoom,idPurpose,fromDate,toDate,breakfastNumber,lunchNumber,dinnerNumber)
	values (2,
		5,
		3,
		3,
		'2018-3-20',
		'2018-3-22',
		4,
		0,
		2);

insert into bookings (idClient,idHotel,idRoom,idPurpose,fromDate,toDate,breakfastNumber,lunchNumber,dinnerNumber)
	values (3,
		1,
		1,
		1,
		'2017-1-1',
		'2017-1-5',
		0,
		0,
		0);

insert into bookings (idClient,idHotel,idRoom,idPurpose,fromDate,toDate,breakfastNumber,lunchNumber,dinnerNumber)
	values (3,
		1,
		1,
		1,
		'2017-1-9',
		'2017-1-12',
		0,
		0,
		0);

insert into bookings (idClient,idHotel,idRoom,idPurpose,fromDate,toDate,breakfastNumber,lunchNumber,dinnerNumber)
	values (3,
		1,
		1,
		1,
		'2017-1-27',
		'2017-1-30',
		0,
		0,
		0);

insert into bookings (idClient,idHotel,idRoom,idPurpose,fromDate,toDate,breakfastNumber,lunchNumber,dinnerNumber)
	values (3,
		1,
		2,
		1,
		'2017-1-19',
		'2017-1-22',
		0,
		0,
		0);

insert into bookings (idClient,idHotel,idRoom,idPurpose,fromDate,toDate,breakfastNumber,lunchNumber,dinnerNumber)
	values (3,
		1,
		3,
		1,
		'2017-1-14',
		'2017-1-17',
		0,
		0,
		0);

insert into bookings (idClient,idHotel,idRoom,idPurpose,fromDate,toDate,breakfastNumber,lunchNumber,dinnerNumber)
	values (3,
		1,
		4,
		1,
		'2017-1-7',
		'2017-1-10',
		0,
		0,
		0);

insert into bookings (idClient,idHotel,idRoom,idPurpose,fromDate,toDate,breakfastNumber,lunchNumber,dinnerNumber)
	values (3,
		1,
		4,
		1,
		'2017-1-20',
		'2017-1-23',
		0,
		0,
		0);

insert into bookings (idClient,idHotel,idRoom,idPurpose,fromDate,toDate,breakfastNumber,lunchNumber,dinnerNumber)
	values (3,
		1,
		5,
		1,
		'2017-1-4',
		'2017-1-8',
		0,
		0,
		0);

insert into bookings (idClient,idHotel,idRoom,idPurpose,fromDate,toDate,breakfastNumber,lunchNumber,dinnerNumber)
	values (3,
		1,
		5,
		1,
		'2017-1-24',
		'2017-1-27',
		0,
		0,
		0);

insert into bookings (idClient,idHotel,idRoom,idPurpose,fromDate,toDate,breakfastNumber,lunchNumber,dinnerNumber)
	values (3,
		1,
		6,
		1,
		'2017-1-1',
		'2017-1-10',
		0,
		0,
		0);

insert into bookings (idClient,idHotel,idRoom,idPurpose,fromDate,toDate,breakfastNumber,lunchNumber,dinnerNumber)
	values (3,
		1,
		7,
		1,
		'2017-1-20',
		'2017-1-30',
		0,
		0,
		0);