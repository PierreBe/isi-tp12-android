package manager;

import java.util.HashMap;

import entity.Hotel;

public class Test {

	public static void main(String[] args) {

		// HotelManager :

		// // getAll() :

		System.out.println(" - HotelManager.getAll() : ");
		/*HashMap<Integer, Hotel> hotels = HotelManager.getAll();
		for (Integer i : hotels.keySet())
			System.out.println(i + " : " + hotels.get(i).getName());
		System.out.println();*/
		for (Hotel h : HotelManager.getAll())
			System.out.println(h.getId() + " : " + h.getName());
		System.out.println();

		// // ! getByCity

		// // ! getByState

		// // ! getByCountry

		// // getCities() :

		System.out.println(" - HotelManager.getCities() : ");
		for (String s : HotelManager.getCities())
			System.out.println(s);
		System.out.println();

		// // getCitiesByState() :

		System.out.println(" - HotelManager.getCitiesByState(\"Québec\") : ");
		for (String s : HotelManager.getCitiesByState("Québec"))
			System.out.println(s);
		System.out.println();

		// // getCitiesByCountry() :

		System.out.println(" - HotelManager.getCitiesByCountry(\"Canada\") : ");
		for (String s : HotelManager.getCitiesByCountry("Canada"))
			System.out.println(s);
		System.out.println();

		// // getStates() :

		System.out.println(" - HotelManager.getStates() : ");
		for (String s : HotelManager.getStates())
			System.out.println(s);
		System.out.println();

		// // getStatesByCountry() :

		System.out.println(" - HotelManager.getStatesByCountry(\"Canada\") : ");
		for (String s : HotelManager.getStatesByCountry("Canada"))
			System.out.println(s);
		System.out.println();

		// // getCountries() :

		System.out.println(" - HotelManager.getCountries() : ");
		for (String s : HotelManager.getCountries())
			System.out.println(s);
		System.out.println();

		// test deserialisation :

		System.out.println(" - deserialisation : ");

		String s = Hotel.serialize(HotelManager.getAll());
		System.out.println(s);
		for (Hotel h : Hotel.deserializeArray(s))
			System.out.println(h.getId());

	}

}
