package controller;

import java.io.IOException;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import manager.HotelManager;

@WebServlet("/cities")
public class CityController extends HttpServlet {

	private static final long serialVersionUID = 1L;

	public CityController() {
		super();
	}

	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {

		String[] cities;

		if (request.getParameter("state") != null && !request.getParameter("state").isEmpty())
			cities = HotelManager.getCitiesByState(request.getParameter("state"));
		else if (request.getParameter("country") != null && !request.getParameter("country").isEmpty())
			cities = HotelManager.getCitiesByCountry(request.getParameter("country"));
		else
			cities = HotelManager.getCities();

		response.getWriter().append(String.join(Resources.SEPARATOR_ARRAY, cities));

	}

	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		doGet(request, response);
	}

}
