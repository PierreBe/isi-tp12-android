package controller;

import java.io.IOException;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import entity.Booking;
import manager.BookingManager;

@WebServlet("/bookings")
public class BookingController extends HttpServlet {

	private static final long serialVersionUID = 1L;

	public BookingController() {
		super();
	}

	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {

		Booking[] bookings;

		if (request.getParameter("booking") != null && !request.getParameter("booking").isEmpty()) {
			bookings = new Booking[1];
			bookings[0] = BookingManager.create(request.getParameter("booking"));
		} else
			bookings = BookingManager.getByAnyParameter(request.getParameterMap());

		response.getWriter().append(bookings[0] == null ? null : Booking.serialize(bookings));

	}

	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		doGet(request, response);
	}

}
