package controller;

import java.io.IOException;
import java.io.PrintWriter;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import entity.Picture;
import manager.PictureManager;

@WebServlet("/test")
public class TestController extends HttpServlet {

	private static final long serialVersionUID = 1L;

	public TestController() {
		super();
	}

	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {

		PrintWriter pw = response.getWriter();

		pw.append("<body>");

		Picture[] pictures;

		if (request.getParameter("id") != null && !request.getParameter("id").isEmpty())
			pictures = PictureManager.getById(request.getParameter("id"));
		else if (request.getParameter("idRoom") != null && !request.getParameter("idRoom").isEmpty())
			pictures = PictureManager.getByIdRoom(request.getParameter("idRoom"));
		else
			pictures = PictureManager.getAll();

		for (Picture p : pictures)
			pw.append("<div><img src=\"" + p.getUrl() + "\" alt=\"" + p.getUrl() + "\"/></div>\n");

		pw.append("</body>");

	}

}
